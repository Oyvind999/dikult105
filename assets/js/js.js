/* 
Code by Cand 142 
*/

$("document").ready(function() {

	// Handle view recipes click:
    $(".recipesLink").click(function() {
        $('html,body').animate({
            scrollTop: $("#recipes").offset().top
        }, 1000);
        return false;
    });
	// handle navbar element click:
    $("nav ul li a").click(function() {
        $("nav ul li a").removeClass('active');
        $(this).addClass('active');
    });

       // handle toTop link click:
    $(".toTop").click(function() {
        $('html,body').animate({
            scrollTop: $("body").offset().top // scroll to top
        }, 500);
        return false;
    });
});

$(document).scroll(function() {
    var y = $(this).scrollTop();
    if (y > 100) {
        $('#toTopDesktop').fadeIn();
    } else {
        $('#toTopDesktop').fadeOut();
    }
});

$(function() {
    // get hash value
    var hash = window.location.hash;
    //  scroll to element
    $('html, body').animate({
        scrollTop: $(hash).offset().top
    });
});
